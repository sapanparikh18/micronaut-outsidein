package user.registration;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Post;

@Controller("/users")
public class UsersController {

    private UsersService usersService;

    public UsersController(UsersService usersService) {

        this.usersService = usersService;
    }

    @Get("/")
    public HttpStatus index() {
        return HttpStatus.OK;
    }


    @Post
    public Response<User> save(@Body User user) {
        return usersService.save(user);
    }
}
