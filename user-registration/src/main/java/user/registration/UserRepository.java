package user.registration;

import java.util.UUID;
import javax.inject.Singleton;

@Singleton
public class UserRepository {

  User save(User user){
    user.setId(UUID.randomUUID().toString());
    return user;
  }
}
