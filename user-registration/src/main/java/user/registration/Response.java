package user.registration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Response<T> {
  private T data;
  private String error;
  public enum Status{
    FAIL,
    SUCCESS
  }
  private Status status;
  public T getData() {
    return data;
  }

  public String getError() {
    return error;
  }
  @JsonCreator
  public Response(@JsonProperty("data") T data,@JsonProperty("error") String error,@JsonProperty("status") Status status) {
    this.data = data;
    this.error = error;
    this.status = status;
  }

  public Status getStatus() {
    return status;
  }
}
