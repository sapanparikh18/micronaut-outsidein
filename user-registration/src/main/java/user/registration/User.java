package user.registration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

  private final String name;
  private final String address;
  private final String mobileNumber;
  private String id;
  @JsonCreator
  public User(@JsonProperty("name") String name,@JsonProperty("address") String address,@JsonProperty("mobile_number") String mobileNumber) {
    this.name = name;
    this.address = address;
    this.mobileNumber = mobileNumber;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
