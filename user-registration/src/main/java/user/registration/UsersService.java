package user.registration;

import javax.inject.Singleton;
import user.registration.Response.Status;

@Singleton
public class UsersService {

  private UserRepository repository;

  public UsersService(UserRepository repository) {
    this.repository = repository;
  }

  public Response<User> save(User user) {
    User savedUser = repository.save(user);
    return new Response<>(savedUser,"", Status.SUCCESS);
  }
}
