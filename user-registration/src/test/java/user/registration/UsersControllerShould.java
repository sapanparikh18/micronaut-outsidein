package user.registration;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;

class UsersControllerShould {

  UsersService usersService = mock(UsersService.class);
  UsersController usersController = new UsersController(usersService);
  User user = new User("Sapan Parikh","Address", "9099952762");
  @Test
  public void use_user_service_to_save_user(){
    Response<User> userResponse = usersController.save(user);
    verify(usersService).save(user);
  }

}
