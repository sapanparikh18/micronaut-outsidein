package user.registration;

import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
public class UsersControllerTest {

    @Inject
    EmbeddedServer embeddedServer;

    @Test
    public void save_user_and_retrieve() throws Exception {
        try(RxHttpClient client = embeddedServer.getApplicationContext().createBean(RxHttpClient.class, embeddedServer.getURL())) {
            User user = new User("Sapan Parikh","Address", "9099952762");
            Response<User> response = client.toBlocking().retrieve(HttpRequest.POST("/users",user),
                Argument.of(Response.class,Argument.of(User.class)));
            Assertions.assertThat(response.getData().getId().length()).isGreaterThan(0);

        }
    }
}
