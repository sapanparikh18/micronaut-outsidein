package user.registration;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class UsersServiceShould {
  UserRepository repository = mock(UserRepository.class);
  UsersService usersService = new UsersService(repository);
  User user = new User("John Smith","Address", "8623718055");
  @Test
  public void call_repository_to_save_user(){
      usersService.save(user);
      verify( repository).save(user);
  }

}
